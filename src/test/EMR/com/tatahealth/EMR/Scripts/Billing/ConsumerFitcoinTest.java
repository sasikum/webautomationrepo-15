package com.tatahealth.EMR.Scripts.Billing;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ConsumerFitcoinTest {
	ConsultationTest appt=new ConsultationTest();
	BillingTest bs=new BillingTest();
	Billing bill = new Billing();
	ConsultationPage consultation = new ConsultationPage();
	ConsumerAppointmentTest consumer= new ConsumerAppointmentTest();
	public static ExtentTest logger;
	
	
	@BeforeClass(alwaysRun = true) public static void beforeClass() throws Exception { 
		  Reports.reports();
		  Login_Doctor.executionName="ConsumerFitcoinTest";
		  BillingTest.doctorName="Topaa";
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
		  }
	  
	  @AfterClass(alwaysRun = true) public static void afterClass() throws Exception {
		  Login_Doctor.LogoutTest();
			Reports.extent.flush(); 
		  }
	
	/*Billing_C_PP_2-pay at clinic is enabled from doctor side*/
	/*  Mention test case covered in all test below :RGB_5 ,VB_1-VB_24,Biiling_C_P_27-Biiling_C_P_33,Biiling_C_P_18,Biiling_C_P_23-Biiling_C_P_26
	 * Biiling_C_P_47-Biiling_C_P_48
	 */
	  

		/*Test case:Billing_C_PP_9*/
		@Test(priority=75,groups = {"G1"})
		public void consumerFPaymentCashWalletEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Fitcoin Payment Emr Validation with Cash Wallet");
			String paymentCheck="Cash, Wallet, Rewards";
			String paidStatus="Cash, Wallet";
			consumer.consumerBookAppointment("Fit Coin");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeFStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer. consumerFBillingPageValidation();
			 bs.paymentCashWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashWalletPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeFStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashWalletOption();
		  bs.billingCreatePaymentOptionDisplay();
		  bs.paymentCashWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashWalletPdfValidation();
		 
		}
		
		@Test(priority=76,groups = {"G3"})
		public void consumerFPaymentCashDebitCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Fitcoin Payment Emr Validation with Cash Debit Card");
			String paymentCheck="Cash, Debit card, Rewards";
			String paidStatus="Cash, Debit card";
			consumer.consumerBookAppointment("Fit Coin");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeFStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerFBillingPageValidation();
			 bs.paymentCashDebitCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeFStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs.billingCreatePaymentOptionDisplay();
		  bs.paymentCashDebitCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardPdfValidation();
		 
		}
		@Test(priority=77,groups = {"G2"})
		public void consumerFPaymentCashCreditCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Fitcoin Payment Emr Validation with Cash Credit Card");
			String paymentCheck="Cash, Credit card, Rewards";
			String paidStatus="Cash, Credit card";
			consumer.consumerBookAppointment("Fit Coin");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeFStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerFBillingPageValidation();
			 bs.paymentCashCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeFStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs.billingCreatePaymentOptionDisplay();
		  bs.paymentCashCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardPdfValidation();
		 
		}
		
		@Test(priority=78,groups = {"G2"})
		public void consumerFPaymentCashEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Fitcoin Payment Emr Validation with Cash");
			String paymentCheck="Cash, Rewards";
			String paidStatus="Cash";
			consumer.consumerBookAppointment("Fit Coin");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeFStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer. consumerFBillingPageValidation();
			 bs.paymentCash();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeFStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCash();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashPdfValidation();
		 
		}
		@Test(priority=79,groups = {"G3"})
		public void consumerFPaymentCreditCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Fitcoin Payment Emr Validation with Credit Card");
			String paymentCheck="Credit card, Rewards";
			String paidStatus="Credit card";
			consumer.consumerBookAppointment("Fit Coin");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeFStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerFBillingPageValidation();
			 bs.paymentCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeFStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCardPdfValidation();
		 
		}

		@Test(priority=80,groups = {"G1"})
		public void consumerFPaymentWalletEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Fitcoin Payment Emr Validation with Wallet");
			String paymentCheck="Wallet, Rewards";
			String paidStatus="Wallet";
			consumer.consumerBookAppointment("Fit Coin");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeFStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer. consumerFBillingPageValidation();
			 bs.paymentWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingWalletPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeFStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingWalletPdfValidation();
		 
		}

}
