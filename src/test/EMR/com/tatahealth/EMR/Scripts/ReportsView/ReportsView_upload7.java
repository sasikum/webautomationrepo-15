package com.tatahealth.EMR.Scripts.ReportsView;

import java.util.Random;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.pages.ReportsView.ReportsView;

public class ReportsView_upload7 {
	ReportsView report = new ReportsView();
	public static ExtentTest logger;
	Random random = new Random();
	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "reportsViewUpload";
		Login_Doctor.LoginTestwithDiffrentUser("Kasu");
	}
	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}
	public synchronized void reportsViewUploadLunch() throws Exception {
		logger = Reports.extent.createTest("EMR_Reports View/Upload Page");
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
		report.getEmrTopMenuElement(Login_Doctor.driver, logger, "Reports").click();
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
	}
	@Test(priority = 55, groups = { "Regression", "ReportView" })
	public void closePatientDetialsModal() throws Exception {
		logger = Reports.extent.createTest("To check whether user can be able to close patient detail pop up from detailed view  report page");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		String UHID = report.getMobileNumber(Login_Doctor.driver, logger).getText();
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(UHID);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		report.getViewReports(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(1);
		report.getPatientIcon(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		report.getPatientDetailsCloseBtn(Login_Doctor.driver, logger).click();
		try {
			Assert.assertTrue(report.getPatientIcon(Login_Doctor.driver, logger).isDisplayed(), "User image/profile in detailed report page ");
		} catch (Exception e) {
			Assert.assertTrue(false);
		}	
	}
	@Test(priority = 56, groups = { "Regression", "ReportView" })
	public void miscellaneousDisplay() throws Exception {
		logger = Reports.extent.createTest("Verify the list of requests while doing consultation when doctor enters lab results");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		report.getViewReports(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(1);
		Assert.assertEquals(report.getPatientsTestName(Login_Doctor.driver, logger).getText(), "Miscellaneous");
	}
	@Test(priority = 57, groups = { "Regression", "ReportView" })
	public void miscellaneousLabTestDisplay() throws Exception {
		logger = Reports.extent.createTest("Verify the list of requests while doing consultation when doctor enters lab results");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		report.getViewReports(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(1);
		Assert.assertEquals(report.getPatientsTestName(Login_Doctor.driver, logger).getText(), "Miscellaneous");	
	}
	@Test(priority = 59, groups = { "Regression", "ReportView" }, enabled = false)
	public void uploadPopup() throws Exception {
		logger = Reports.extent.createTest("To check when user click on View/Upload Report in detailed report view page");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(4);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		report.getViewReports(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(1);
		report.getUploadReportLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		try {
			report.getCloseIconUploadReport(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			Assert.assertTrue(report.getUploadReportPopup(Login_Doctor.driver, logger).isDisplayed(), "View/Upload Report popup is displayed");	
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	@Test(priority = 61, groups = { "Regression", "ReportView" }, enabled = false)
	public void closeUploadReportPopUp() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to close upload reports pop up");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);	
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		report.getViewReports(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(1);
		report.getUploadReportLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		try {
			report.getCloseIconUploadReport(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			Assert.assertTrue(report.getPatientIcon(Login_Doctor.driver, logger).isDisplayed(), "View/Upload Report popup closed and Patient icon displayed is displayed");
		} catch (Exception e) {
			Assert.assertTrue(false);
		}
		Web_GeneralFunctions.wait(3);
	}
	@Test(priority = 62, groups = { "Regression", "ReportView" }, enabled = false)
	public void checkUploadEnabled() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to upload reports manually in upload reports pop up page");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		report.getViewReports(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(1);
		report.getUploadReportLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		report.getSelectFileBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);	
		try {
			Web_GeneralFunctions.uploadFile("\\src\\test\\resources\\Report.jpeg");
			Web_GeneralFunctions.wait(2);
			Assert.assertTrue(report.getUploadBtn(Login_Doctor.driver, logger).isEnabled());
			Web_GeneralFunctions.wait(2);
			report.getCloseIconUploadReport(Login_Doctor.driver, logger).click();
		} catch (Exception e) {
			Assert.assertTrue(false);
		}
	}	
	@Test(priority = 62, groups = { "Regression", "ReportView" }, enabled = false)
	public void checkUploadReportBtnEnabled() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to upload reports manually in upload reports pop up page");
		Login_Doctor.driver.get("https://emrcstg.tatahealth.com/reports");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		report.getViewReports(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(1);
		report.getUploadReportLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		report.getSelectFileBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		try {
			Web_GeneralFunctions.uploadFile("/src/test/resources/Report.jpeg");
			Web_GeneralFunctions.wait(2);
			Assert.assertTrue(report.getUploadBtn(Login_Doctor.driver, logger).isEnabled());
			report.getUploadBtn(Login_Doctor.driver, logger).click();
			Assert.assertTrue(report.getUploadedFiles(Login_Doctor.driver, logger).isDisplayed());
			Web_GeneralFunctions.wait(2);
			report.getCloseIconUploadReport(Login_Doctor.driver, logger).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test(priority = 63, groups = { "Regression", "ReportView" },enabled = false)
	public void checkReportVideoUpload() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to upload video in upload reports pop up");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		report.getViewReports(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(1);
		report.getUploadReportLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		report.getSelectFileBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);	
		try {
			Web_GeneralFunctions.uploadFile("/src/test/resources/VideoReport.mp4");
			Web_GeneralFunctions.wait(2);
			String actualAlertMessage = report.getUploadAlertText(Login_Doctor.driver, logger).getText();
			Assert.assertEquals(actualAlertMessage, "This file type is not allowed!");
			report.getFileUpoadAcceptBtn(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			report.getCloseIconUploadReport(Login_Doctor.driver, logger).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
