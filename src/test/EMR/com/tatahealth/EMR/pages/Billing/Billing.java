package com.tatahealth.EMR.pages.Billing;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class Billing {
	public WebElement getEmrTopMenu(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//*[@id='topMenuIcon']/a", driver, logger);
 		
	}
	
	public WebElement getEmrTopMenuElement(WebDriver driver,ExtentTest logger,String emrElement) 
	{
		String emrElementxpath="//*[@id='sidebarlinks']/li[contains(.,'"+emrElement+"')]";
		return Web_GeneralFunctions.findElementbyXPath(emrElementxpath, driver, logger);
 		
	}
	
	public WebElement getEmrBillTab(WebDriver driver,ExtentTest logger,String emrBill) 
	{
		String emrElementxpath="//*[@id='myTabs']/li[contains(.,'"+emrBill+"')]";
		return Web_GeneralFunctions.findElementbyXPath(emrElementxpath, driver, logger);
 			
	}
	 public WebElement getEmrBillErrorMessage(WebDriver driver,ExtentTest logger){
			
		 return Web_GeneralFunctions.findElementbyXPath("//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']", driver, logger);	
		}
	
	//Create Bill Page WebElement
	public WebElement getEmrCreateBillPatientTextBox(WebDriver driver,ExtentTest logger) 
	{
		 Web_GeneralFunctions.findElementbyXPath("//label[contains(.,'Select Patient')]", driver, logger).isDisplayed();
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='searchbillpatientquick']", driver, logger);
 		
	}
	public WebElement getEmrCreateBillPatientSearchNoResulFoundText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//ul[@id='patientsearchresult' and contains(.,'No result found')]", driver, logger);	
	}
	public List<WebElement> getEmrCreateBillPatientSearchResultSelect(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.listOfElementsbyXpath("//*[@id='patientsearchresult']/li/div[contains(.,'Select')]", driver, logger);

	}
	public WebElement getEmrCreateBillDoctorTextBox(WebDriver driver,ExtentTest logger) 
	{
		 Web_GeneralFunctions.findElementbyXPath("//label[contains(.,'Select doctor')]", driver, logger).isDisplayed();
		 return Web_GeneralFunctions.findElementbyXPath("//input[@id='doctorSearchField']", driver, logger);
	}
	public WebElement getEmrCreateBillDoctorSearchNoResulFoundText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//li[contains(.,'No result found')]", driver, logger);	
	}
	
	public List<WebElement> getEmrCreateBillDoctorSearchResult(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.listOfElementsbyXpath("//*[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front']/li", driver, logger);	
	}
	public WebElement getEmrCreateBillServiceSectionText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//h2[contains(.,'Service')]", driver, logger);	
	}
	public WebElement getEmrCreateBillServiceColumnText(WebDriver driver,ExtentTest logger,String serviceText) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//table[@id='serviceTable']/thead/tr/th[contains(.,'"+serviceText+"')]", driver, logger);
	}
	public WebElement getEmrCreateBillServiceColumnTextBox(WebDriver driver,ExtentTest logger,String serviceTextBox,int serviceTextBoxindex) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//*[@id='"+serviceTextBox+""+serviceTextBoxindex+"']", driver, logger);
	}
	public WebElement getEmrCreateBillServiceSearchMatchingServiceFoundText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//li[contains(.,'No matching service found')]", driver, logger);	
	}
	public List<WebElement> getEmrCreateBillServiceList(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.listOfElementsbyXpath("//*[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front']/li", driver, logger);
	}
	public WebElement getEmrCreateBillPaymentModeText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//h4[contains(.,'Payment Mode')]", driver, logger);
	}
	public WebElement getEmrCreateBillPayementOption(WebDriver driver,ExtentTest logger,String paymentoption) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//label[@class='checkbox-inline payments-heading' and contains(.,'"+paymentoption+"')]/input", driver, logger);
	}
	public WebElement getEmrCreateBillGenerateButton(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//a[contains(.,'Generate Bill')]", driver, logger);
	}
	public WebElement getEmrCreateBillCardOption(WebDriver driver,ExtentTest logger,String cardOption) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//label[@class='radio-inline' and contains(.,'"+cardOption+"')]", driver, logger);
	}

	public static WebElement getEmrOrganisation(WebDriver driver,ExtentTest logger,String organisation) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//*[@class='globalclinicbox' and contains(.,'"+organisation+"')]", driver, logger);	
	}
	public WebElement getEmrGenerateBillSuccessMessage(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("(//div[@id='successPrintBill']//h4)[1]", driver, logger);
	}
	public WebElement getEmrGenerateBillCloseButton(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("(//button[contains(.,' Close')])[1]", driver, logger);
	}
	
	public WebElement getEmrGenerateBillPrintButton(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("(//button[contains(.,' Print Bill')])[1]", driver, logger);
	}
	public WebElement getEmrGeneratePatientNameBillNumber(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("(//p[@id='popUpSuccessMessage'])[1]", driver, logger);	
	}
	public WebElement getEmrCreateBillActionAddButton(WebDriver driver,ExtentTest logger) 
	{
		return  Web_GeneralFunctions.findElementbyXPath("//button[@id='addBillingRow']", driver, logger);
	}
	public WebElement getEmrCreateBillActionDeleteButton(WebDriver driver,ExtentTest logger,int rowNumber) 
	{
		return Web_GeneralFunctions.findElementbyXPath("(//button[@class='btn btn-danger deleteBillingRow'])["+rowNumber+"+1]", driver, logger);
	}
	
	public WebElement getEmrCreateBillService(WebDriver driver,ExtentTest logger,String service,int rowNumber) 
	{
		return  Web_GeneralFunctions.findElementbyXPath("(//li[contains(.,'"+service+"')])["+rowNumber+"+1]", driver, logger);
	}
	public WebElement getEmrCreateBillAdditionalDiscountPercentageText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//label[@for='AddDiscPer']", driver, logger);
	}
	
	public WebElement getEmrCreateBillAdditionalDiscountAmountText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//label[@for='AddDiscAmt']", driver, logger);
	}
	public WebElement getEmrCreateBillPaidOnlineText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//label[@for='paidOnline']", driver, logger);
	}
	public WebElement getEmrCreateBillAmountPayableText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//label[@for='amountPayable']", driver, logger);
	}
	public WebElement getEmrCreateBillGrandTotalText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//label[@for='grandTotal']", driver, logger);	
	}
	
	public WebElement getEmrCreateBillAdditionalDiscountPercentageTextBox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='AddDiscPer']", driver, logger);
	}
	public WebElement getEmrCreateBillAdditionalDiscountAmountTextBox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='AddDiscAmt']", driver, logger);
	}
	public WebElement getEmrCreateBillPaidOnlineTextBox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='paidOnline']", driver, logger);	
	}
	public WebElement getEmrCreateBillAmountPayableTextBox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='amountPayable']", driver, logger);
	}
	public WebElement getEmrCreateBillGrandTotalTextBox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='grandTotal']", driver, logger);	
	}
	public WebElement getEmrCreateBillServiceTotalText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='grandTotal']", driver, logger);
 			
	}
	public WebElement getEmrCreateBillServiceValue(WebDriver driver,ExtentTest logger,String serviceValue) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//th[@id='"+serviceValue+"']", driver, logger);
 			
	}
	public WebElement getEmrCreateBillPayementModeTextBox(WebDriver driver,ExtentTest logger,String paymentMode) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='"+paymentMode+"']", driver, logger);
	}
     
	//View Bill WebElement
	
	public WebElement getEmrViewBillSearchText(WebDriver driver,ExtentTest logger,String searchText) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//label[contains(.,'"+searchText+"')]", driver, logger);
	}
	public WebElement getEmrViewBillPatientNameSearchTextBox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='patientName']", driver, logger);
	}
	public WebElement getEmrViewBillAgeSearchTextBox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='age']", driver, logger);
	}
	public WebElement getEmrViewBillUhIdSearchTextBox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='uhid']", driver, logger);
	}
	public WebElement getEmrViewBillClinicIdSearchTextBox(WebDriver driver,ExtentTest logger) 
	{
		return  Web_GeneralFunctions.findElementbyXPath("//input[@id='clinicId']", driver, logger);
	}
	public WebElement getEmrViewBillMobileNoSearchTextBox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='mobileNo']", driver, logger);
	}
	public WebElement getEmrViewBillGovtIdSearchTextBox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='govtId']", driver, logger);	
	}
	public void getEmrViewBillGenderDropdown(WebDriver driver,ExtentTest logger,String value) throws Exception  
	{
		 Web_GeneralFunctions.findElementbyXPath("//*[@id='gender']", driver, logger).click();
		 Web_GeneralFunctions.findElementbyXPath("//Select[@id='gender']/option[contains(.,'Male')]", driver, logger).isDisplayed();
		 Web_GeneralFunctions.findElementbyXPath("//Select[@id='gender']/option[contains(.,'Female')]", driver, logger).isDisplayed();
		 Web_GeneralFunctions.findElementbyXPath("//Select[@id='gender']/option[contains(.,'"+value+"')]", driver, logger).click();
}
	public WebElement getEmrViewBillFromDateSearchTextBox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//div[@id='billingFromDate']/input", driver, logger);	
	}
	public WebElement getEmrViewBillFromDateSearcCalendarIcon(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//div[@id='billingFromDate']/span", driver, logger);
	}
	public WebElement getEmrViewBillToDateSearchTextBox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//div[@id='billingToDate']/input", driver, logger);
	}
	public WebElement getEmrViewBillToDateSearcCalendarIcon(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//div[@id='billingToDate']/span", driver, logger);

	}
	public WebElement getEmrViewBillSearchBillButton(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//button[contains(.,'Search Bill')]", driver, logger);	
	}
	public WebElement getEmrViewBillTypeDropdown(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//div[@class='form-group margin-billing' and contains(.,'Bill Type')]//button/b", driver, logger);
		
 	}
	public WebElement getEmrViewBillTypeDropdownValue(WebDriver driver,ExtentTest logger,String billType) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//div[@class='form-group margin-billing' and contains(.,'Bill Type')]//ul/li//label[contains(.,'"+billType+"')]", driver, logger);
		
	}
	
	public WebElement getEmrViewSelectDoctorDropdown(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//div[@class='form-group margin-billing' and contains(.,'Select Doctor')]//button/b", driver, logger);
	}
	public WebElement getEmrViewBillSelectDoctorDropdownValue(WebDriver driver,ExtentTest logger,String selectDoctor) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//div[@class='form-group margin-billing' and contains(.,'Select Doctor')]//ul/li//label[contains(.,'"+selectDoctor+"')]/input", driver, logger);
	}
	public WebElement getEmrViewBillNoResultsMessage(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//center[contains(.,'No Results')]", driver, logger);
		
	}
	public WebElement getEmrViewBillSerachResultColumnText(WebDriver driver,ExtentTest logger,String columnText) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//tr[@role='row']/th[contains(.,'"+columnText+"')]", driver, logger);
		
	}
	public List<WebElement> getEmrViewBillSearchResultPrintList(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.listOfElementsbyXpath("//button[@class='btn btn-primary' and contains(.,'Print')]", driver, logger);
	}
	public List<WebElement> getEmrViewBillSearchResultCancelList(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.listOfElementsbyXpath("//button[@class='btn btn-danger']", driver, logger);	
	}
	public WebElement getEmrViewBillCancelMessage(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//p[contains(.,'Are you sure, you want to cancel this bill?')]", driver, logger);
	}
	public WebElement getEmrViewBillCancelYesButton(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//button[@class='confirm' and contains(.,'Yes')]", driver, logger);
	}
	public WebElement getEmrViewBillCancelNoButton(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//button[@class='cancel' and contains(.,'No')]", driver, logger);
	}
	
	public WebElement getEmrCreateBillCancelButton(WebDriver driver,ExtentTest logger) 
	{
		return   Web_GeneralFunctions.findElementbyXPath("//a[@class='btn btn-danger' and contains(.,'Cancel')]", driver, logger);
	}
	public WebElement getEmrCreateBillSaveDraftButton(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//a[@class='btn btn-success' and contains(.,'Save as draft')]", driver, logger);
	}
	public List<WebElement> getEmrViewBillSearchResultRowList(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.listOfElementsbyXpath("//div[@id='viewBillingTable_wrapper']//tbody/tr", driver, logger);	
	}
	public WebElement getEmrViewBillSearchResultColumnVaildation(WebDriver driver,ExtentTest logger,int rowNumber,int columnIndex) 
	{
		return   Web_GeneralFunctions.findElementbyXPath("//div[@id='viewBillingTable_wrapper']//tbody/tr["+rowNumber+"]/td["+columnIndex+"]", driver, logger);
	}
	public WebElement getEmrViewBillSearchResultNextButton(WebDriver driver,ExtentTest logger) 
	{
		return   Web_GeneralFunctions.findElementbyXPath("//*[@id='viewBillingTable_next']", driver, logger);
	}
	public WebElement getEmrCreateBillAppliedBenefitText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//div[@id='createBillTable']//div[@class='form-inline row col-md-12']", driver, logger);
	}
	
	public WebElement getTataHealthLogoLink(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//a[@id='powredByLink']/img", driver, logger);
	}
	

	}
